class Config:
    TEST_IMAGE_PATH = "data/new_images"
    TEST_LABLE_PATH =  "data/new_labels"
    TEST_IMAGE_FILE = 'data/train.txt'
    TRAIN_IMAGE_PATH = "data/new_images"
    TRAIN_LABEL_PATH = "data/new_labels"
    TRAIN_IMAGE_FILE = 'data/train.txt'
    USE_C = 1
    RESTORE = 0
    LEARNING_RATE = 0.0001
    DECAY = 0.9
    ITERS_PER_DECAY = 1
    TOTAL_EPOCHS = 160
    BATCHSIZE = 1
    TOTAL_SAMPLES = 184
    GPU_DEVICE_NUM ='0'


